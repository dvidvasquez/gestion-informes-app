const express = require('express');
const jwt = require('jsonwebtoken');

const Informe = require('../models/informesModel.js');
const config = require('../config.js');

let router = express.Router();

router.get('/', (req, res) => {
    Informe.find({}, (err, informes) => {
        res.json({ informes });
    })
});

router.post('/add', (req, res) => {
    const name = req.body.name || '';
    const img = req.body.img || '';
    const text = req.body.text || '';
    const pdfurl = req.body.pdfurl || '';
    const views = req.body.views;
    const category = req.body.category;

    // const { isValid, errors } = checkForErrors({ name, img, text });

    if (true) {
        const newInforme = new Informe({
            name: name,
            img: img,
            text: text,
            pdfurl: pdfurl,
            views: views,
            category: category
        });

        newInforme.save((err) => {
            if (err) throw err;
            else {
                res.json({ success: 'success' });
            }
        });
    } else {
        res.json({ errors });
    }
});

router.post('/edit/:id', (req, res) => {
    const name = req.body.name || '';
    const img = req.body.img || '';
    const text = req.body.text || '';
    const pdfurl = req.body.pdfurl || '';
    const views = req.body.views;
    const category = req.body.category;

    if (true) {
        const updatedInforme = {
            name: name,
            img: img,
            text: text,
            pdfurl: pdfurl,
            views: views,
            category: category
        };

        Informe.findByIdAndUpdate(req.params.id, updatedInforme, err => {
            if (err) throw err;
            else res.json({ success: 'success' });
        });
    } else {
        res.json({ errors });
    }
});

module.exports = router;
