const mongoose = require('mongoose');

const InformeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    img: {
        type: String,
        required: true
    },
    text: {
        type: String,
        unique: true,
        required: true
    },
    pdfurl: {
        type: String,
        required: true
    },
    views: {
        type: Number
    },
    category: {
        type: String,
        required: true
    }
});

const Informe = mongoose.model('Informe', InformeSchema)

module.exports = Informe;
