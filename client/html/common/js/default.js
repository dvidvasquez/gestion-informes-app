'use strict';
// var API_URL = location.protocol + '//' + location.host + '/coordinadoraWebServices/';
// var API_URL = 'http://rutacvirtual.coordinadora.com/coordinadoraWebServices/'; student_id = 'admin';

// OperationsScorm();

(function() {
    $('#parallax').parallax();

    setTimeout(function() {
        $(".coo-coverpage-box>h4").css('display', 'block').addClass('animated fadeInDown');
        $(".coo-coverpage-box>p").css('display', 'block').addClass('animated fadeInUp');
    }, 1000);

    setTimeout(function() {
        $(".coo-coverpage").addClass('animated fadeOutLeft');

    }, 4000);

    setTimeout(function() {
        $(".coo-coverpage").css('z-index', '0');
        $(".coo-instrucciones").css('opacity', '1').addClass('animated fadeIn');
    }, 4500);
})();

$('.coo-close-intro').on('click', function() {
    $('.coo-instrucciones').removeClass('fadeIn').addClass('fadeOut').css({'opacity':'0', 'z-index' : '0'});
});

$('#coo-menu').
on('click', function() {
    if ($('#coo-content-menu').hasClass('coo-hide-content-menu')) {
        $('#coo-content-menu').removeClass('coo-hide-content-menu').fadeIn().addClass('coo-show-content-menu');
    } else if ($('#coo-content-menu').hasClass('coo-show-content-menu')) {
        $('#coo-content-menu').removeClass('coo-show-content-menu').fadeOut();
        setTimeout(function() {
            $('#coo-content-menu').addClass('coo-hide-content-menu');
        }, 1000);
    }
});

$('#coo-content-menu').find('li').on('click', function() {
    PageTransitions.nextPage({ animation: 37, showPage: $(this).data('page') });
});

$('ul.dl-menu>li').on('click', function() {
    $('audio').each(function(){
        this.pause(); // Stop playing
        this.currentTime = 0; // Reset time
    }); 
});

$('#coo-save').on('click', function() {
	if (ok) {
	    var currentPage;
	    $('.pt-page').each(function(index, el) {
	        if ($(el).hasClass('pt-page-current')) {
	            currentPage = index.toString();
	        }
	    });

	    ScormProcessSetValue("cmi.core.lesson_location", currentPage);
	}
});

$('#coo-exit').on('click', function() {
    window.history.back();
});

$('.coo-video').find('.coo-img').prepend('<img src="../common/img/previewVideos.png" alt="">');

$('.coo-video').on('click', function() {
    var $this = $(this);

    var urlVideo = $this.data('vid'),
        nameVideo = $(this).data('title');
    if (urlVideo) {
        $.popup({youtubeID: urlVideo, target: $('.coo-full-template'), title: nameVideo, onLoad: function(elems) { closeAtEnd(elems); } });
        $('.lnt-popup-content').addClass('coo-padpop-video');
    }
});

$('.popup-text').on('click', function() {
    var $this = $(this),
        idPop = $this.data('pop');

    if (idPop) {
        $.popup({
            id : idPop, 
            target: $('.coo-full-template')
        });        
    }

    $('.lnt-popup-content').attr('style', 'height:450px');

});

$('.coo-audio').on('click', function(){
    var $this = $(this),
        audio = $this.data('audio');

    if (audio){
        var src = 'media/' + audio;
        var au = $('body').find('audio').length;
        if (au > 0){
            $('#audio').remove();
            var tag = '<audio id="audio"><source src="'+src+'"></audio>';
            $('body').append(tag);
        } else {
            var tag = '<audio id="audio"><source src="'+src+'"></audio>';
            $('body').append(tag);
        }

        var aud = document.getElementById('audio');
        aud.volume = 1;
        aud.play();
    }
});

$(document.body).on('click', '#coo-btn-cerrar', function() {
    top.close();
});