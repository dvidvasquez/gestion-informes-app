var bookmark
    ,student_id
    ,ok;

function OperationsScorm() {
    iniciarLMS();


    function iniciarLMS() {

        ok = ScormProcessInitialize();

        if (ok) {

            bookmark = ScormProcessGetValue("cmi.core.lesson_location");
            student_id = ScormProcessGetValue("cmi.core.student_id");

            if (bookmark != "") {

                if (confirm("¿Desea continuar donde guardo antes?")) {
                    console.log("Quedaste en la página: " + bookmark);
                    PageTransitions.nextPage({ animation: 37, showPage: bookmark });

                }
            } else {
                console.log("No se trajo el valor guardado");
            }
        }
    }

    window.onbeforeunload = function() {
        if (ok) {
        	saveAndClose();
        }
    };

    function saveAndClose() {
        var currentPage;
        $('.pt-page').each(function(index, el) {
            if ($(el).hasClass('pt-page-current')) {
                currentPage = index.toString();
            }
        });

        ScormProcessSetValue("cmi.core.lesson_location", currentPage);
        ScormProcessFinish();
    }

    window.saveAndClose = saveAndClose
}
