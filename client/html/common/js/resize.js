var pageWidth, pageHeight;

var basePage = {
    width: 1366,
    height: 768,
    scale: 1,
    scaleX: 1,
    scaleY: 1
};

$(function() {
    var $page = $('.coo-full-template');

    getPageSize();
    scalePages($page, pageWidth, pageHeight);

    //using underscore to delay resize method till finished resizing window
    $(window).on('resize', function() {
        getPageSize();
        scalePages($page, pageWidth, pageHeight);
    });


    function getPageSize() {
        pageHeight = $('#coo-flexible-box').height();
        pageWidth = $('#coo-flexible-box').width();
    }

    function scalePages(page, maxWidth, maxHeight) {
        var scaleX = 1,
            scaleY = 1;
        scaleX = maxWidth / basePage.width;
        scaleY = maxHeight / basePage.height;
        basePage.scaleX = scaleX;
        basePage.scaleY = scaleY;
        basePage.scale = (scaleX > scaleY) ? scaleY : scaleX;

        var newLeftPos = Math.abs(Math.floor(((basePage.width * basePage.scale) - maxWidth) / 2));
        var newTopPos = Math.abs(Math.floor(((basePage.height * basePage.scale) - maxHeight) / 2));

        page.attr('style', 'transform:scale(' + basePage.scale + ');transform-origin:0px 0px;left:' + newLeftPos + 'px;top:' + newTopPos + 'px;');
    }
});

(function Menu() {
    var pages = $('.pt-page')
    ,pagesCount = pages.length;

    pages.each(function(i){
        var $this = $(this)
            ,nav = $($('#menu').html())
            ,progressbar = nav.find('.coo_progress_progressbar');

        $this.append(nav);
        
        if (i != 0) {
            nav.find('.coo-arrow-left').append('<div class="dl-menu">\
                                                    <ul class="dl-menu">\
                                                        <li data-animation="26" data-page="' + (i-1) + '"></li>\
                                                    </ul>\
                                                </div>');
        } else {
            nav.find('.coo-arrow-left').addClass('coo-arrow-blocked');
        }

        if (i != pagesCount - 1) {
            nav.find('.coo-arrow-right').append('<div class="dl-menu">\
                                                    <ul class="dl-menu">\
                                                        <li data-animation="26" data-page="' + (i+1) + '"></li>\
                                                    </ul>\
                                                </div>');
        } else {
            nav.find('.coo-arrow-right').addClass('coo-arrow-blocked');
        }
        
        for(var j = 0; j < pagesCount; j++) {
            var li = $('<li>').css('width', 100/pagesCount + '%')
                            .data('page', j)
                            .css('cursor', 'pointer')
                            .on('click', function(){
                                PageTransitions.nextPage({animation: 26, showPage: $(this).data('page') })
                            });

            if (j == i)
                li.addClass('coo_progress_active')

            progressbar.append(li);
        }

        nav.find('.coo-page').append('<span>' + (i+1) + '</span>');
    })
})();

function closeAtEnd(elems) {
    var ifr = elems.popup.find('iframe').get(0);
    var player = new Vimeo.Player(ifr);
    player.on('ended', function() {
        setTimeout(function(){
            elems.popup.remove();
        }, 200);
    });
}
