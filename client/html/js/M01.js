var informes = [];
var storiesViewed = 0;

// axios.get('http://www.vtsas.co/gestionback/api/informes/')
// .then(function (response) {
//     informes = response.data['rta'];
//     const filtrado = filtrarInformes('Materiales Avanzados');
//     writeBoxes(filtrado);
// })
// .catch(function (error) {
//     console.log(error);
// });

setTimeout(()=>{
    var response = {"rta":[{"id":1,"name":"Boletin Global Materiales Compuestos","img":"01","text":"Los materiales compuestos son algunos de los elementos m\u00e1s vers\u00e1tiles creados por el hombre, estos pueden satisfacer necesidades tan diversas gracias a que ellos mismos son muy diversos.","pdfurl":"1_boletin_global_materiales_compuestos.pdf","category":"Materiales Avanzados","views":0},{"id":2,"name":"Boletin Especifico Materiales Compuestos","img":"01","text":"Para la profundizaci\u00f3n de contenidos en la segunda iteraci\u00f3n realizada en el marco de IaaS, se realiz\u00f3 una priorizaci\u00f3n de necesidades identificadas respecto a nuevos materiales, especialmente desde su valor agregado a los clientes.","pdfurl":"2_boletin_especifico_materiales_compuestos.pdf","category":"Materiales Avanzados","views":0}]}

    informes = response['rta'];
    const filtrado = filtrarInformes('Materiales Avanzados');
    writeBoxes(filtrado);
},
1000)


function filtrarInformes(category){
    const informesFiltrados = informes.filter(informe => informe.category === category);
    return informesFiltrados;
}

function editarInforme(id,informe){
    axios({
            method: 'post',
            url: `http://www.vtsas.co/gestionback/api/informes/edit/${id}`,
            data: informe,
            headers: {
              'Content-Type' : 'application/json'
            }
          })
          .then(function (response) {
            return response.data
          })
          .catch(function (response) {});
}

function scaleItemsNext() {
    var index = -1;
    var items = $('.owl-item').each(function (i) {
        var $this = $(this).css('transform', 'none');

        if (index == -1 && $this.hasClass('active'))
            index = i;
    });

    items.eq(index + 0).css({
        'transform': 'scale(0.7)'
    });
    items.eq(index + 1).css({
        'transform': 'scale(0.8)'
    });
    items.eq(index + 2).css({
        'transform': 'scale(0.9)'
    });
    items.eq(index + 3).css({
        'transform': 'scale(0.8)'
    });
    items.eq(index + 4).css({
        'transform': 'scale(0.7)'
    });

    document.querySelectorAll('.owl-item').forEach((element)=>{
        if(!element.classList.contains('active')){
            element.style.opacity = '0'
        } else {
            element.style.opacity = '1'
        }
    })
    

}

function scaleItemsPrev() {
    var index = -1;
    var items = $('.owl-item').each(function (i) {
        var $this = $(this).css('transform', 'none');

        if (index == -1 && $this.hasClass('active'))
            index = i;
    });

    items.eq(index - 0).css({
        'transform': 'scale(0.7)'
    });
    items.eq(index + 1).css({
        'transform': 'scale(0.8)'
    });
    items.eq(index + 2).css({
        'transform': 'scale(0.9)',
    });
    items.eq(index + 3).css({
        'transform': 'scale(0.8)'
    });
    items.eq(index + 4).css({
        'transform': 'scale(0.7)'
    });

    document.querySelectorAll('.owl-item').forEach((element)=>{
        if(!element.classList.contains('active')){
            element.style.opacity = '0'
        } else {
            element.style.opacity = '1'
        }
    })
    
}

function writeBoxes(array) {

    $('#owl-demo').html(" ");
    $("#owl-demo").trigger('destroy.owl.carousel');

    $(array).each(function (i, e) {
        var content;
        if (e.pdfurl == '' || !e.pdfurl) {
            content = '' +
                '<div class="item" data-url="' + e.pdfurl + '">' +
                '<h2>' + e.name + '</h2>' +
                '<img src="img/prev.png" alt="">' +
                '<span>' + e.text + '</span>' +
                '</div>';
        } else {
            content = '' +
                '<div class="item" data-info-id="' + e.id + '" data-url="' + e.pdfurl + '">' +
                '<h2>' + e.name + '</h2>' +
                '<img src="img/' + e.img + '.png" alt="img-pdf">' +
                '<i class="views"></i><p class="p-view">'+e.views+'</p>'  +
                '<span>' + e.text + '</span>' +
                '</div>';
        }
        $('#owl-demo').append(content);
    });
    
    document.querySelectorAll('.item span').forEach(element => {
        let text = element.textContent
        let newText = text.substring(0, 80)
        if(text.length > 71){
        newText = newText + '...'
        }
        element.textContent = newText
        });

    $("#owl-demo").owlCarousel({
        items: 5,
        mouseDrag: false,
        touchDrag: false,
        nav: true,
        navText: [
            '<div id="navArrows">' +
            '<div class="left"><img src="img/arrow.png"></div>' +
            '</div>',
            '<div id="navArrows">' +
            '<div class="right"><img src="img/arrow.png"></div>' +
            '</div>'
        ], // Show next and prev buttons
        dots: false,
        //autoWidth : true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        center: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            750: {
                items: 3
            },
            1000: {
                items: 5
            }
        },
        responsiveRefreshRate: 300
    });

    $('.owl-next').trigger('click');

    $(document.body).on('click', '.center>.item', function () {
        var pdfurl = $(this).data('url')
        var name = $(this).find('h2').text();
        $.popup({
            content: `<iframe src="pdfs/${pdfurl}" style="width:1000px; height:600px;" frameborder="0"></iframe>`,
            width: 'auto',
            title: name,
            target: $('.coo-full-template')
        });
        $('.lnt-popup-content').addClass('coo-padpop-video');
    });
    
    $(document.body).on('click', '.owl-next', function (event) {
        scaleItemsNext();
    });
    
    $(document.body).on('click', '.owl-prev', function (event) {
        scaleItemsPrev();
    });
}

document.querySelector('#categories').addEventListener('change', function (event) {
    let category = document.getElementById("categories").value;
    const filtrados = filtrarInformes(category);
    writeBoxes(filtrados);
});

$(document.body).on('click', '.center>.item', function (e) {
    var pdfurl = $(this).data('url');
    var infoId = $(this).data('info-id');
    var name = $(this).find('h2').text();    
    $.popup({
        content: `<iframe src="pdfs/${pdfurl}" style="width:1000px; height:600px;" frameborder="0"></iframe>`,
        width: 'auto',
        title: name,
        target: $('.coo-full-template')
    });
    $('.lnt-popup-content').addClass('coo-padpop-video');

    const informe = informes.filter(informe => informe.id === infoId);
    informe[0].views += 1;
    editarInforme(infoId,informe[0]);
    e.target.parentElement.querySelector('.p-view').innerHTML = informe[0].views;
});

$(document.body).on('click', '.owl-next', function (event) {
    scaleItemsNext();
});

$(document.body).on('click', '.owl-prev', function (event) {
    scaleItemsPrev();
});