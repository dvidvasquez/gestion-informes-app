import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './containers/Home/Home';
import Signup from './containers/Users/Signup/Signup';
import Login from './containers/Users/Login/Login';
import FullArticle from './containers/Articles/FullArticle/FullArticle';
import AddArticle from './containers/Articles/AddArticle/AddArticle';
import EditArticle from './containers/Articles/EditArticle/EditArticle';
import NavigationBar from './containers/NavigationBar/NavigationBar';
import Informes from './containers/Informes/Informes';

class App extends Component {
    render() {
        return (
            <div className="container-fluid" style={{padding: '0px'}}>
                <NavigationBar />
                <Switch>
                    <Route exact path="/article/add" component={AddArticle} />
                    <Route path="/article/edit/:id" component={EditArticle} />
                    <Route path="/articles/:id" component={FullArticle} />
                    <Route path="/login" component={Login} />
                    <Route path="/signup" component={Signup} />
                    <Route path="/informes" component={Informes} />
                    {/* <Route path="/" component={Home} /> */}
                    <Route path="/" component={Login} />
                </Switch>
                <footer>
                    <img src="./img/logos.png" />
                </footer>
            </div>
        );
    }
}

export default App;
